#ifndef STREAM9_LIBMOUNT_TABLE_HPP
#define STREAM9_LIBMOUNT_TABLE_HPP

#include "cstring_view.hpp"
#include "fs.hpp"
#include "namespace.hpp"
#include "opt.hpp"

#include <iterator>
#include <ranges>

#include <libmount.h>

namespace stream9::libmount {

class fs;
class table_iterator;
class child_range;

class table
{
public:
    using iterator = table_iterator;
    using sentinel = std::default_sentinel_t;

public:
    // essential
    table();
    explicit table(::libmnt_table&) noexcept; // take ownership

    static table from_file(cstring_view filename);
    static table from_dir(cstring_view dirname);

    ~table() noexcept;

    table(table const&) noexcept;
    table& operator=(table const&) noexcept;

    table(table&&) noexcept;
    table& operator=(table&&) noexcept;

    friend void swap(table&, table&) noexcept;

    // accessor
    iterator begin() const;
    sentinel end() const noexcept;

    iterator rbegin() const;
    sentinel rend() const noexcept;

    child_range children(fs const& parent) const;

    // query
    int size() const noexcept;
    bool empty() const noexcept;

    void* userdata() const noexcept;

    bool with_comments() const noexcept;
    opt<cstring_view> intro_comment() const noexcept;
    opt<cstring_view> trailing_comment() const noexcept;

    opt<class cache> cache() const noexcept;

    int find_fs(fs const&) const noexcept;
    fs first_fs() const noexcept;
    fs last_fs() const noexcept;
    opt<fs/*child*/> over_fs(fs const& parent) const;
    fs root_fs() const;

    opt<fs> find_mountpoint(cstring_view path, int direction = MNT_ITER_FORWARD) const noexcept;
    opt<fs> find_target(cstring_view path, int direction = MNT_ITER_FORWARD) const noexcept;
    opt<fs> find_srcpath(cstring_view path, int direction = MNT_ITER_FORWARD) const noexcept;
    opt<fs> find_tag(cstring_view tag,
                     cstring_view val, int direction = MNT_ITER_FORWARD) const noexcept;
    opt<fs> find_target_with_option(cstring_view path,
                                    cstring_view option,
                                    cstring_view val,
                                    int direction = MNT_ITER_FORWARD) const noexcept;
    opt<fs> find_source(cstring_view source, int direction = MNT_ITER_FORWARD) const noexcept;
    opt<fs> find_pair(cstring_view source,
                      cstring_view target,
                      int direction) const noexcept;
    opt<fs> find_devno(::dev_t devno, int direction = MNT_ITER_FORWARD) const noexcept;

    bool is_fs_mounted(fs const&) const noexcept;

    // modifier
    void parse_stream(FILE*, cstring_view filename);
    void parse_file(cstring_view filename);
    void parse_dir(cstring_view dirname);

    void parse_fstab(opt<cstring_view> filename = {});
    void parse_swaps(opt<cstring_view> filename = {});
    void parse_mtab(opt<cstring_view> filename = {});

    void add_fs(fs const&);
    void insert_fs(bool before, fs const* pos, fs const&);
    void remove_fs(fs const&);

    void uniq_fs(int flags,
        int (*cmp)(struct ::libmnt_table*,
                   struct ::libmnt_fs*,
                   struct ::libmnt_fs* ));

    void set_userdata(void* data) noexcept;

    void enable_comments(bool enable) noexcept;
    void set_intro_comment(cstring_view comm);
    void append_intro_comment(cstring_view comm);
    void set_trailing_comment(cstring_view comm);
    void append_trailing_comment(cstring_view comm);

    void set_cache(class cache) noexcept;

    void set_parser_errcb(
        int (*cb)(struct ::libmnt_table*, char const* filename, int line) ) noexcept;

    void reset();

    // conversion
    operator ::libmnt_table* () const noexcept { return m_handle; }

private:
    ::libmnt_table* m_handle;
};

void move_fs(table& src, table& dst, bool before, fs const* pos, fs);

void write_file(table&, FILE* file);
void replace_file(table&, cstring_view filename);

} // namespace stream9::libmount

#if 0
/* tab.c */
// std::find_if will suffice
extern int mnt_table_find_next_fs(struct libmnt_table *tb,
			struct libmnt_iter *itr,
			int (*match_func)(struct libmnt_fs *, void *),
			void *userdata,
		        struct libmnt_fs **fs);
#endif

#endif // STREAM9_LIBMOUNT_TABLE_HPP

#include "iterator.hpp"
#include "child_range.hpp"

