#ifndef STREAM9_LIBMOUNT_ERROR_HPP
#define STREAM9_LIBMOUNT_ERROR_HPP

#include <stream9/errors.hpp>

#include <system_error>

namespace stream9::libmount {

using stream9::error;

std::error_category const& category() noexcept;

inline std::error_code
make_error_code(int const ec) noexcept
{
    return { ec, error_category() };
}

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_ERROR_HPP
