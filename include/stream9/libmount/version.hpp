#ifndef STREAM9_LIBMOUNT_VERSION_HPP
#define STREAM9_LIBMOUNT_VERSION_HPP

#include "cstring_view.hpp"

#include <span>

namespace stream9::libmount {

int parse_version_string(cstring_view ver_string) noexcept;

int get_library_version(cstring_view*) noexcept;

std::span<cstring_view> get_library_features() noexcept;

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_VERSION_HPP
