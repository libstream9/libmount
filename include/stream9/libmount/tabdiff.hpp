#ifndef STREAM9_LIBMOUNT_TABDIFF_HPP
#define STREAM9_LIBMOUNT_TABDIFF_HPP

#include "fs.hpp"
#include "json.hpp"
#include "table.hpp"

#include <iosfwd>
#include <iterator>
#include <memory>

#include <libmount.h>

#include <stream9/ranges/range_facade.hpp>

namespace stream9::libmount {

class diff_iterator;

class tabdiff : public stream9::ranges::range_facade<tabdiff>
{
public:
    // essential
    tabdiff(table const& old_tab, table const& new_tab);

    ~tabdiff() noexcept;

    tabdiff(tabdiff const&) = delete;
    tabdiff& operator=(tabdiff const&) = delete;

    tabdiff(tabdiff&&) noexcept;
    tabdiff& operator=(tabdiff&&) noexcept;

    friend void swap(tabdiff&, tabdiff&) noexcept;

    // accessor
    diff_iterator           begin() const;
    std::default_sentinel_t end() const noexcept;

    diff_iterator           rbegin() const;
    std::default_sentinel_t rend() const noexcept;

    // conversion
    operator ::libmnt_tabdiff* () const noexcept { return m_handle; }

private:
    ::libmnt_tabdiff* m_handle;
};

struct diff_result {
    opt<fs> old_fs;
    opt<fs> new_fs;
    int oper;
};

void tag_invoke(json::value_from_tag, json::value&, diff_result const&);

std::ostream& operator<<(std::ostream&, diff_result const&);

class diff_iterator : public itr::iterator_facade<diff_iterator,
                                std::input_iterator_tag,
                                diff_result const& >
{
public:
    diff_iterator() = default;
    diff_iterator(tabdiff const&, int direction);

private:
    friend class itr::iterator_core_access;

    diff_result const& dereference() const noexcept
    {
        return m_val;
    }

    void increment();

    bool equal(std::default_sentinel_t) const noexcept
    {
        return m_end;
    }

private:
    ::libmnt_tabdiff* m_diff {};
    std::shared_ptr<::libmnt_iter> m_it {};
    diff_result m_val {};
    bool m_end = false;
};

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_TABDIFF_HPP
