#ifndef STREAM9_LIBMOUNT_UMOUNT_HPP
#define STREAM9_LIBMOUNT_UMOUNT_HPP

#include "fs.hpp"

namespace stream9::libmount {

class context;
class iter;

void umount(context&);

struct next_umount_result {
    class fs fs;
    int mntrc;
    int ignored;
};

next_umount_result next_umount(context&, iter&);

fs find_umount_fs(context&, cstring_view tgt);

} // namespace stream9::libmount

#if 0
/* context_umount.c */
extern int mnt_context_prepare_umount(struct libmnt_context *cxt)
			__ul_attribute__((warn_unused_result));
extern int mnt_context_do_umount(struct libmnt_context *cxt);
extern int mnt_context_finalize_umount(struct libmnt_context *cxt);
#endif

#endif // STREAM9_LIBMOUNT_UMOUNT_HPP
