#ifndef STREAM9_LIBMOUNT_LOCK_HPP
#define STREAM9_LIBMOUNT_LOCK_HPP

#include "cstring_view.hpp"

#include <libmount.h>

namespace stream9::libmount {

class lock
{
public:
    // essential
    lock(cstring_view datafile, ::pid_t id = 0);

    ~lock() noexcept;

    lock(lock const&) = delete;
    lock& operator=(lock const&) = delete;

    lock(lock&&) noexcept;
    lock& operator=(lock&&) noexcept;

    friend void swap(lock&, lock&) noexcept;

    // modifier
    void block_signals(bool enable) noexcept;

    // command
    void lock_file();
    void unlock_file() noexcept;

    // conversion
    operator ::libmnt_lock* () const noexcept { return m_handle; }

private:
    ::libmnt_lock* m_handle;
};

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_LOCK_HPP
