#ifndef STREAM9_LIBMOUNT_UTILS_HPP
#define STREAM9_LIBMOUNT_UTILS_HPP

#include "cstring.hpp"
#include "cstring_view.hpp"
#include "opt.hpp"

namespace stream9::libmount {

class cache;

cstring mangle(cstring_view str);
cstring unmangle(cstring_view str);

bool tag_is_valid(cstring_view tag) noexcept;
bool fstype_is_netfs(cstring_view type) noexcept;
bool fstype_is_pseudofs(cstring_view type) noexcept;

bool match_fstype(opt<cstring_view> type, opt<cstring_view> pattern) noexcept;
bool match_options(opt<cstring_view> optstr, opt<cstring_view> pattern) noexcept;

cstring_view get_fstab_path() noexcept;
cstring_view get_swaps_path() noexcept;
cstring_view get_mtabs_path() noexcept;

bool has_regular_mtab(cstring_view* mtab, bool* writable) noexcept;

cstring get_mountpoint(cstring_view path);

opt<cstring> guess_system_root(::dev_t devno, cache*);

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_UTILS_HPP
