#ifndef STREAM9_LIBMOUNT_NAMESPACE_HPP
#define STREAM9_LIBMOUNT_NAMESPACE_HPP

namespace stream9::strings {}
namespace stream9::iterators {}

namespace stream9::libmount {

namespace str { using namespace stream9::strings; }
namespace itr { using namespace stream9::iterators; }

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_NAMESPACE_HPP
