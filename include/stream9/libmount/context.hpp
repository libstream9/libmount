#ifndef STREAM9_LIBMOUNT_CONTEXT_HPP
#define STREAM9_LIBMOUNT_CONTEXT_HPP

#include "cstring_view.hpp"
#include "opt.hpp"

#include <span>

#include <libmount.h>

namespace stream9::libmount {

class cache;
class fs;
class table;

class context
{
public:
    // essential
    context();

    ~context() noexcept;

    context(context const&) = delete;
    context& operator=(context const&) = delete;

    context(context&&) noexcept;
    context& operator=(context&&) noexcept;

    friend void swap(context&, context&) noexcept;

    // query
    opt<cstring_view> source() const noexcept;
    opt<cstring_view> target() const noexcept;
    opt<cstring_view> fstype() const noexcept;
    opt<cstring_view> target_prefix() const noexcept;
    opt<cstring_view> options() const noexcept;

    bool is_restricted() const noexcept;
    bool is_lazy() const noexcept;
    bool is_rdonly_umount() const noexcept;
    bool is_rwonly_mount() const noexcept;
    bool is_sloppy() const noexcept;
    bool is_fake() const noexcept;
    bool is_nomtab() const noexcept;
    bool is_force() const noexcept;
    bool is_verbose() const noexcept;
    bool is_loopdel() const noexcept;
    bool is_nohelpers() const noexcept;
    bool is_nocanonicalize() const noexcept;
    bool is_swapmatch() const noexcept;
    bool forced_rdonly() const noexcept;
    bool is_fork() const noexcept;
    bool is_parent() const noexcept;
    bool is_child() const noexcept;

    bool tab_applied() const noexcept;

    unsigned long mflags() const;
    unsigned long user_mflags() const;

    bool is_fs_mounted(class fs const&) const;

    int optsmode() const noexcept;

    class fs fs() const;
    void* fs_userdata() const noexcept;

    class table fstab() const;
    void* fstab_userdata() const noexcept;

    class table mtab() const;
    void* mtab_userdata() const noexcept;

    opt<class cache> cache() const noexcept;

    void wait_for_children(int* nchildren, int* nerrs) const noexcept;

    class table table(cstring_view filename) const;

    bool status() const noexcept;

    bool helper_executed() const noexcept;
    int helper_status() const noexcept;

    bool syscall_called() const noexcept;
    int syscall_errno() const noexcept;

    int excode(int rc, std::span<char> buf) const noexcept;

    ::libmnt_lock* lock() const noexcept;

    ::libmnt_ns* target_ns() const noexcept;
    ::libmnt_ns* origin_ns() const noexcept;

    // modifier
    void set_source(cstring_view source);
    void set_target(cstring_view target);
    void set_fstype(cstring_view fstype);
    void set_target_prefix(cstring_view path);

    void set_options(cstring_view optstr);
    void append_options(cstring_view optstr);

    void enable_lazy(bool enable) noexcept;
    void enable_rdonly_umount(bool enable) noexcept;
    void enable_rwonly_mount(bool enable) noexcept;
    void enable_sloppy(bool enable) noexcept;
    void enable_fake(bool enable) noexcept;
    void disable_mtab(bool disable) noexcept;
    void enable_force(bool enable) noexcept;
    void enable_verbose(bool enable) noexcept;
    void enable_loopdel(bool enable) noexcept;
    void disable_helpers(bool disable) noexcept;
    void disable_canonicalize(bool disable) noexcept;
    void disable_swapmatch(bool disable) noexcept;
    void enable_fork(bool enable) noexcept;

    void set_mflags(unsigned long flags);
    void set_user_mflags(unsigned long flags);

    void set_optsmode(int mode) noexcept;

    void set_fs(class fs const&) noexcept;
    void set_fstab(class table const&) noexcept;

    void set_cache(class cache const&) noexcept;

    void set_fstype_pattern(cstring_view pattern);
    void set_options_pattern(cstring_view pattern);
    void set_mountdata(void* data) noexcept;

    void force_unrestricted() noexcept;

    void init_helper(int action, int flags) noexcept;
    bool helper_setopt(int c, char* arg) noexcept;

    int apply_fstab() noexcept;

    void reset() noexcept;
    void reset_status() noexcept;

    void set_syscall_status(int status) noexcept;

    void set_tables_errcb(
        int (*cb)(struct ::libmnt_table* tb, char const* filename, int line) ) noexcept;

    void set_target_ns(cstring_view path);

    ::libmnt_ns* switch_origin_ns() noexcept;
    ::libmnt_ns* switch_target_ns() noexcept;

    // conversion
    operator ::libmnt_context* () const noexcept { return m_handle; }

private:
    ::libmnt_context* m_handle;
};

} // namespace stream9::libmount
#if 0
extern int mnt_context_set_passwd_cb(struct libmnt_context *cxt,
			      char *(*get)(struct libmnt_context *),
			      void (*release)(struct libmnt_context *, char *))
			__ul_attribute__((deprecated));

extern int mnt_context_strerror(struct libmnt_context *cxt, char *buf,
				size_t bufsiz)
				__ul_attribute__((deprecated));
#endif

#endif // STREAM9_LIBMOUNT_CONTEXT_HPP
