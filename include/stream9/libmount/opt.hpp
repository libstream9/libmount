#ifndef STREAM9_LIBMOUNT_OPT_HPP
#define STREAM9_LIBMOUNT_OPT_HPP

#include <optional>

namespace stream9::libmount {

template<typename T>
using opt = std::optional<T>;

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_OPT_HPP
