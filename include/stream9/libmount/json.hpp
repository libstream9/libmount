#ifndef STREAM9_LIBMOUNT_JSON_HPP
#define STREAM9_LIBMOUNT_JSON_HPP

#include "namespace.hpp"

namespace stream9::json {

struct value_from_tag;

class value;

} // namespace stream9::json;

#endif // STREAM9_LIBMOUNT_JSON_HPP
