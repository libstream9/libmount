#ifndef STREAM9_LIBMOUNT_CHILD_RANGE_HPP
#define STREAM9_LIBMOUNT_CHILD_RANGE_HPP

#include "fs.hpp"
#include "iterator.hpp"
#include "namespace.hpp"
#include "table.hpp"

#include <iterator>

#include <libmount.h>

#include <stream9/ranges/range_facade.hpp>

namespace stream9::libmount {

class child_iterator;

class child_range : public stream9::ranges::range_facade<child_range>
{
public:
    child_range(table const& t, fs const& parent) noexcept
        : m_table { t }
        , m_parent { parent }
    {}

    child_iterator begin() const;

    std::default_sentinel_t end() const noexcept
    {
        return {};
    }

    child_iterator rbegin() const;

    std::default_sentinel_t rend() const noexcept
    {
        return {};
    }

private:
    table m_table {};
    fs m_parent {};
};

class child_iterator : public itr::iterator_facade<child_iterator,
                                    std::input_iterator_tag,
                                    fs >
{
public:
    // essential
    child_iterator() = default;

    child_iterator(table const& t, fs const& parent, int direction);

    // query
    int direction() const noexcept;

    // modifier
    void set(fs const& f);
    void reset() noexcept;

private:
    friend class itr::iterator_core_access;

    fs dereference() const
    {
        return m_val;
    }

    void increment();

    bool equal(std::default_sentinel_t) const noexcept
    {
        return m_end;
    }

private:
    table m_table {};
    fs m_parent {};
    std::shared_ptr<::libmnt_iter> m_it {};
    fs m_val {};
    bool m_end = false;
};

inline child_iterator child_range::
begin() const
{
    return { m_table, m_parent, MNT_ITER_FORWARD };
}

inline child_iterator child_range::
rbegin() const
{
    return { m_table, m_parent, MNT_ITER_BACKWARD };
}

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_CHILD_RANGE_HPP
