#ifndef STREAM9_LIBMOUNT_MONITOR_HPP
#define STREAM9_LIBMOUNT_MONITOR_HPP

#include "cstring_view.hpp"
#include "opt.hpp"

#include <chrono>

#include <libmount.h>

namespace stream9::libmount {

class monitor
{
public:
    using duration = std::chrono::duration<int, std::milli>;

public:
    // essential
    monitor();

    ~monitor() noexcept;

    monitor(monitor const&) noexcept;
    monitor& operator=(monitor const&) noexcept;

    monitor(monitor&&) noexcept;
    monitor& operator=(monitor&&) noexcept;

    friend void swap(monitor&, monitor&) noexcept;

    // query
    int fd() const;

    bool wait(duration timeout = duration(-1)) const;

    struct next_change_result {
        cstring_view filename;
        int type;
    };
    opt<next_change_result> next_change() const;

    // modifier
    void enable_kernel(bool enable = true);
    void enable_userspace(bool enable = true, opt<cstring_view> filename = {});

    void cleanup();

private:
    ::libmnt_monitor* m_handle;
};

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_MONITOR_HPP
