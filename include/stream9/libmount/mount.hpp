#ifndef STREAM9_LIBMOUNT_MOUNT_HPP
#define STREAM9_LIBMOUNT_MOUNT_HPP

#include "fs.hpp"

namespace stream9::libmount {

class context;
class iter;

void mount(context&);

struct next_mount_result {
    class fs fs;
    int mntrc;
    int ignored;
};

next_mount_result next_mount(context&, iter&);

next_mount_result next_remount(context&, iter&);

} // namespace stream9::libmount

#if 0
/* context_mount.c */
extern int mnt_context_prepare_mount(struct libmnt_context *cxt)
			__ul_attribute__((warn_unused_result));
extern int mnt_context_do_mount(struct libmnt_context *cxt);
extern int mnt_context_finalize_mount(struct libmnt_context *cxt);
#endif
#endif // STREAM9_LIBMOUNT_MOUNT_HPP
