#ifndef STREAM9_LIBMOUNT_ITERATOR_HPP
#define STREAM9_LIBMOUNT_ITERATOR_HPP

#include "fs.hpp"
#include "namespace.hpp"
#include "table.hpp"

#include <iterator>
#include <memory>

#include <libmount.h>

#include <stream9/iterators.hpp>

namespace stream9::libmount {

class table_iterator : public itr::iterator_facade<table_iterator,
                                    std::input_iterator_tag,
                                    fs >
{
public:
    // essential
    table_iterator() = default;

    table_iterator(table const& t, int const direction);

    // query
    int direction() const noexcept;

    // modifier
    void set(fs const&);
    void reset() noexcept;

private:
    friend class itr::iterator_core_access;

    fs dereference() const noexcept
    {
        return m_val;
    }

    void increment();

    bool equal(std::default_sentinel_t) const noexcept
    {
        return m_end;
    }

private:
    table m_table {};
    std::shared_ptr<::libmnt_iter> m_it {};
    fs m_val {};
    bool m_end = false;
};

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_ITERATOR_HPP
