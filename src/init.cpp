#include <stream9/libmount/init.hpp>

#include <libmount.h>

namespace stream9::libmount {

void
init_debug(int const mask) noexcept
{
    ::mnt_init_debug(mask);
}

} // namespace stream9::libmount
