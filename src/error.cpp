#include <stream9/libmount/error.hpp>

#include <libmount.h>

#include <stream9/linux/error.hpp>

namespace stream9::libmount {

std::error_category const&
error_category() noexcept
{
    static struct impl : std::error_category {
        char const* name() const noexcept { return "stream9::libmount"; }

        std::string message(int const code) const
        {
            switch (code) {
                case MNT_ERR_NOFSTAB: return "MNT_ERR_NOFSTAB";
                case MNT_ERR_NOFSTYPE: return "MNT_ERR_NOFSTYPE";
                case MNT_ERR_NOSOURCE: return "MNT_ERR_NOSOURCE";
                case MNT_ERR_LOOPDEV: return "MNT_ERR_LOOPDEV";
                case MNT_ERR_APPLYFLAGS: return "MNT_ERR_APPLYFLAGS";
                case MNT_ERR_AMBIFS: return "MNT_ERR_AMBIFS";
                case MNT_ERR_LOOPOVERLAP: return "MNT_ERR_LOOPOVERLAP";
                case MNT_ERR_LOCK: return "MNT_ERR_LOCK";
                case MNT_ERR_NAMESPACE: return "MNT_ERR_NAMESPACE";
            }

            return linux::error_category().message(code);
        }
    } instance;

    return instance;
}

} // namespace stream9::libmount
