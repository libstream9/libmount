#include <stream9/libmount/cache.hpp>

#include <stream9/libmount/error.hpp>
#include <stream9/libmount/table.hpp>

#include <utility>

namespace stream9::libmount {

cache::
cache()
    : m_handle { ::mnt_new_cache() }
{
    if (m_handle == nullptr) {
        throw error { "mnt_new_cache()", make_error_code(errno) };
    }
}

cache::
cache(::libmnt_cache& h) noexcept
    : m_handle { &h }
{}

cache::
~cache() noexcept
{
    if (m_handle) {
        ::mnt_unref_cache(m_handle);
    }
}

cache::
cache(cache const& other) noexcept
    : m_handle { other.m_handle }
{
    ::mnt_ref_cache(m_handle);
}

cache& cache::
operator=(cache const& other) noexcept
{
    cache tmp { other };
    swap(*this, tmp);

    return *this;
}

cache::
cache(cache&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

cache& cache::
operator=(cache&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(cache& lhs, cache& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_handle, rhs.m_handle);
}

bool cache::
device_has_tag(cstring_view const devname,
               cstring_view const token,
               cstring_view const value) const noexcept
{
    assert(m_handle);

    return ::mnt_cache_device_has_tag(m_handle, devname, token, value);
}

cstring_view cache::
find_tag_value(cstring_view const devname,
               cstring_view const token) const noexcept
{
    assert(m_handle);

    return ::mnt_cache_find_tag_value(m_handle, devname, token);
}

void cache::
set_targets(table& mtab) noexcept
{
    assert(m_handle);

    ::mnt_cache_set_targets(m_handle, mtab);
}

bool cache::
read_tags(cstring_view const devname)
{
    assert(m_handle);

    auto const rc = ::mnt_cache_read_tags(m_handle, devname);
    if (rc == 0) {
        return false;
    }
    else if (rc == 1) {
        return true;
    }
    else {
        assert(rc < 0);
        throw error { "mnt_cache_read_tags", make_error_code(-rc) };
    }
}

opt<cached_get_fstype_result>
get_fstype(cstring_view const devname, cache& c) noexcept
{
    int ambi {};
    opt<cached_get_fstype_result> result;

    auto* const ptr = ::mnt_get_fstype(devname, &ambi, c);
    if (ptr) {
        result.emplace(ptr, ambi != 0);
    }

    return result;
}

opt<get_fstype_result>
get_fstype(cstring_view const devname) noexcept
{
    int ambi {};
    opt<get_fstype_result> result;

    auto* const ptr = ::mnt_get_fstype(devname, &ambi, nullptr);
    if (ptr) {
        result.emplace(cstring(ptr), ambi != 0);
    }

    return result;
}

opt<cstring_view>
resolve_path(cstring_view const path, cache& c) noexcept
{
    opt<cstring_view> result;

    auto* const ptr = ::mnt_resolve_path(path, c);
    if (ptr) {
        result.emplace(ptr);
    }

    return result;
}

opt<cstring>
resolve_path(cstring_view const path) noexcept
{
    opt<cstring> result;

    auto* const ptr = ::mnt_resolve_path(path, nullptr);
    if (ptr) {
        result.emplace(cstring(ptr));
    }

    return result;
}

opt<cstring_view>
resolve_target(cstring_view const path, cache& c) noexcept
{
    opt<cstring_view> result;

    auto* const ptr = ::mnt_resolve_target(path, c);
    if (ptr) {
        result.emplace(ptr);
    }

    return result;
}

opt<cstring>
resolve_target(cstring_view const path) noexcept
{
    opt<cstring> result;

    auto* const ptr = ::mnt_resolve_target(path, nullptr);
    if (ptr) {
        result.emplace(cstring(ptr));
    }

    return result;
}

opt<cstring_view>
resolve_tag(cstring_view const token, cstring_view const value, cache& c) noexcept
{
    opt<cstring_view> result;

    auto* const ptr = ::mnt_resolve_tag(token, value, c);
    if (ptr) {
        result.emplace(ptr);
    }

    return result;
}

opt<cstring>
resolve_tag(cstring_view const token, cstring_view const value) noexcept
{
    opt<cstring> result;

    auto* const ptr = ::mnt_resolve_tag(token, value, nullptr);
    if (ptr) {
        result.emplace(cstring(ptr));
    }

    return result;
}

opt<cstring_view>
resolve_spec(cstring_view const spec, cache& c) noexcept
{
    opt<cstring_view> result;

    auto* const ptr = ::mnt_resolve_spec(spec, c);
    if (ptr) {
        result.emplace(ptr);
    }

    return result;
}

opt<cstring>
resolve_spec(cstring_view const spec) noexcept
{
    opt<cstring> result;

    auto* const ptr = ::mnt_resolve_spec(spec, nullptr);
    if (ptr) {
        result.emplace(cstring(ptr));
    }

    return result;
}

opt<cstring>
pretty_path(cstring_view const path, cache& c) noexcept
{
    opt<cstring> result;

    auto* const ptr = ::mnt_pretty_path(path, c);
    if (ptr) {
        result.emplace(cstring(ptr));
    }

    return result;
}

opt<cstring>
pretty_path(cstring_view const path) noexcept
{
    opt<cstring> result;

    auto* const ptr = ::mnt_pretty_path(path, nullptr);
    if (ptr) {
        result.emplace(cstring(ptr));
    }

    return result;
}

} // namespace stream9::libmount
