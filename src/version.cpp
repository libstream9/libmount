#include <stream9/libmount/version.hpp>

#include <libmount.h>

namespace stream9::libmount {

int
parse_version_string(cstring_view const ver_string) noexcept
{
    return ::mnt_parse_version_string(ver_string);
}

int
get_library_version(cstring_view* const ver_string) noexcept
{
    char const* s {};

    auto const v = ::mnt_get_library_version(ver_string ? &s : nullptr);

    if (s) {
        *ver_string = s;
    }

    return v;
}

std::span<cstring_view>
get_library_features() noexcept
{
    char const** features {};

    auto const n = ::mnt_get_library_features(&features);
    assert(n <= 0);

    return {
        reinterpret_cast<cstring_view*>(features),
        static_cast<size_t>(n)
    };
}

} // namespace stream9::libmount
