#ifndef STREAM9_LIBMOUNT_SRC_NAMESPACE_HPP
#define STREAM9_LIBMOUNT_SRC_NAMESPACE_HPP

#include <stream9/libmount/namespace.hpp>

namespace stream9::json {}

namespace stream9::libmount {

namespace json { using namespace stream9::json; }

} // namespace stream9::libmount

#endif // STREAM9_LIBMOUNT_SRC_NAMESPACE_HPP
