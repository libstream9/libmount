#include <stream9/libmount/utils.hpp>

#include <stream9/libmount/cache.hpp>
#include <stream9/libmount/error.hpp>

#include <libmount.h>

#include <stream9/json.hpp>

namespace stream9::libmount {

cstring
mangle(cstring_view const str)
{
    auto* const s = ::mnt_mangle(str);
    if (!s) {
        throw error { "mnt_mangle()", make_error_code(errno) };
    }

    return cstring { s };
}

cstring
unmangle(cstring_view const str)
{
    auto* const s = ::mnt_unmangle(str);
    if (!s) {
        throw error { "mnt_unmangle()", make_error_code(errno) };
    }

    return cstring { s };
}

bool
tag_is_valid(cstring_view const tag) noexcept
{
    return ::mnt_tag_is_valid(tag);
}

bool
fstype_is_netfs(cstring_view const type) noexcept
{
    return ::mnt_fstype_is_netfs(type);
}

bool
fstype_is_pseudofs(cstring_view const type) noexcept
{
    return ::mnt_fstype_is_pseudofs(type);
}

bool
match_fstype(opt<cstring_view> const type,
             opt<cstring_view> const pattern) noexcept
{
    auto const t = type ? static_cast<char const*>(*type) : nullptr;
    auto const p = pattern ? static_cast<char const*>(*pattern) : nullptr;

    return ::mnt_match_fstype(t, p);
}

bool
match_options(opt<cstring_view> const optstr,
              opt<cstring_view> const pattern) noexcept
{
    auto const o = optstr ? static_cast<char const*>(*optstr) : nullptr;
    auto const p = pattern ? static_cast<char const*>(*pattern) : nullptr;

    return ::mnt_match_options(o, p);
}

cstring_view
get_fstab_path() noexcept
{
    return ::mnt_get_fstab_path();
}

cstring_view
get_swaps_path() noexcept
{
    return ::mnt_get_swaps_path();
}

cstring_view
get_mtab_path() noexcept
{
    return ::mnt_get_mtab_path();
}

bool
has_regular_mtab(cstring_view* const mtab, bool* const writable) noexcept
{
    char const* mtab_ {};
    int writable_ {};

    char const** m = mtab ? &mtab_ : nullptr;
    int* w = writable ? &writable_ : nullptr;

    auto const rc = ::mnt_has_regular_mtab(m, w);

    if (mtab) {
        *mtab = mtab_;
    }
    if (writable) {
        *writable = writable_;
    }

    return rc;
}

cstring
get_mountpoint(cstring_view const path)
{
    auto* const s = ::mnt_get_mountpoint(path);
    if (!s) {
        throw error {
            "mnt_get_mountpoint()",
            make_error_code(errno), {
                { "path", path },
            }
        };
    }

    return cstring { s };
}

opt<cstring>
guess_system_root(::dev_t const devno, class cache* const cache)
{
    opt<cstring> result;
    char* path {};
    auto* const c = cache ? static_cast<::libmnt_cache*>(*cache) : nullptr;

    auto const rc = ::mnt_guess_system_root(devno, c, &path);
    if (rc == 0) {
        result.emplace(path);
    }
    if (rc < 0) {
        throw error {
            "mnt_guess_system_root()",
            make_error_code(-rc), {
                { "devno", devno },
            }
        };
    }

    return result;
}

} // namespace stream9::libmount
