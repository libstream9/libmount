#include <stream9/libmount/lock.hpp>

#include <stream9/libmount/error.hpp>

#include <utility>

namespace stream9::libmount {

lock::
lock(cstring_view const datafile, ::pid_t const id/*= 0*/)
    : m_handle { ::mnt_new_lock(datafile, id) }
{
    if (!m_handle) {
        throw error { "mnt_new_lock()", make_error_code(errno) };
    }
}

lock::
~lock() noexcept
{
    if (m_handle) {
        ::mnt_free_lock(m_handle);
    }
}

lock::
lock(lock&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

lock& lock::
operator=(lock&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(lock& lhs, lock& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_handle, rhs.m_handle);
}

void lock::
block_signals(bool enable) noexcept
{
    assert(m_handle);

    ::mnt_lock_block_signals(m_handle, enable);
}

void lock::
lock_file()
{
    auto const rc = ::mnt_lock_file(m_handle);
    if (rc < 0) {
        throw error { "mnt_lock_file()", make_error_code(-rc) };
    }
}

void lock::
unlock_file() noexcept
{
    ::mnt_unlock_file(m_handle);
}

} // namespace stream9::libmount
