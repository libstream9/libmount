#include <stream9/libmount/umount.hpp>

#include <stream9/libmount/context.hpp>
#include <stream9/libmount/error.hpp>
#include <stream9/libmount/iter.hpp>

#include <libmount.h>

namespace stream9::libmount {

void
umount(context& c)
{
    auto const rc = ::mnt_context_umount(c);
    if (rc < 0) {
        throw error { "mnt_context_umount()", make_error_code(-rc) };
    }
}

next_umount_result
next_umount(context& c, iter& i)
{
    ::libmnt_fs* f {};
    int mntrc;
    int ignored;

    auto const rc = ::mnt_context_next_umount(c, i, &f, &mntrc, &ignored);
    if (rc < 0) {
        throw error { "mnt_context_next_umount()", make_error_code(-rc) };
    }

    return {
        fs { *f },
        mntrc,
        ignored
    };
}

fs
find_umount_fs(context& c, cstring_view const tgt)
{
    ::libmnt_fs* f {};

    auto const rc = ::mnt_context_find_umount_fs(c, tgt, &f);
    if (rc < 0) {
        throw error { "mnt_context_find_umount_fs()", make_error_code(-rc) };
    }

    return fs { *f };
}

} // namespace stream9::libmount
