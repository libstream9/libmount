#include <stream9/libmount/fs.hpp>

#include "namespace.hpp"

#include <stream9/libmount/cache.hpp>
#include <stream9/libmount/error.hpp>
#include <stream9/libmount/namespace.hpp>
#include <stream9/libmount/table.hpp>

#include <stream9/json.hpp>

#include <cassert>
#include <utility>

namespace stream9::libmount {

fs::
fs()
    : m_handle { ::mnt_new_fs() }
{
    if (!m_handle) {
        throw error { "mnt_new_fs()", make_error_code(errno) };
    }
}

fs::
fs(::libmnt_fs& h) noexcept
    : m_handle { &h }
{
    ::mnt_ref_fs(m_handle);
}

fs::
~fs() noexcept
{
    if (m_handle) {
        ::mnt_unref_fs(m_handle);
    }
}

fs::
fs(fs const& other) noexcept
    : m_handle { other.m_handle }
{
    assert(other.m_handle);

    ::mnt_ref_fs(m_handle);
}

fs& fs::
operator=(fs const& other) noexcept
{
    fs tmp { other };
    swap(*this, tmp);

    return *this;
}

fs::
fs(fs&& other) noexcept
    : m_handle { other.m_handle }
{
    assert(other.m_handle);

    other.m_handle = nullptr;
}

fs& fs::
operator=(fs&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(fs& lhs, fs& rhs) noexcept
{
    using std::swap;

    swap(lhs.m_handle, rhs.m_handle);
}

opt<class table> fs::
table() const noexcept
{
    assert(m_handle);

    opt<class table> result;
    ::libmnt_table* tbl;

    ::mnt_fs_get_table(m_handle, &tbl);

    if (tbl) {
        result.emplace(*tbl);
    }

    return result;
}

void* fs::
userdata() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_userdata(m_handle);
}

opt<cstring_view> fs::
source() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_source(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
srcpath() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_srcpath(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<fs::tag_t> fs::
tag() const noexcept
{
    assert(m_handle);

    opt<tag_t> result;
    char const* name;
    char const* value;

    auto const rc = ::mnt_fs_get_tag(m_handle, &name, &value);
    if (rc >= 0) {
        result.emplace(name, value);
    }

    return result;
}

opt<cstring_view> fs::
target() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_target(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
fstype() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_fstype(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
options() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_options(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
fs_options() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_fs_options(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
vfs_options() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_vfs_options(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
user_options() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_user_options(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
optional_fields() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_optional_fields(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<std::string_view> fs::
option(cstring_view const name) const
{
    assert(m_handle);

    opt<std::string_view> result;
    char* value {};
    size_t len {};

    auto const rc = ::mnt_fs_get_option(m_handle, name, &value, &len);
    if (rc == 0) {
        result.emplace(value, len);
    }
    else if (rc == 1) {
        // nop
    }
    else {
        assert(rc < 0);
        throw error {
            "mnt_fs_get_option()",
            make_error_code(-rc), {
                { "name", name }
            }
        };
    }

    return result;
}

cstring fs::
strdup_options() const
{
    assert(m_handle);

    auto* const r = ::mnt_fs_strdup_options(m_handle);
    if (r == nullptr) {
        throw error { "mnt_fs_strdup_options", make_error_code(errno) };
    }

    return cstring { r };
}

cstring fs::
vfs_options_all() const
{
    assert(m_handle);

    auto* const r = ::mnt_fs_get_vfs_options_all(m_handle);
    if (r == nullptr) {
        throw error { "mnt_fs_get_vfs_options_all", make_error_code(errno) };
    }

    return cstring { r };
}

unsigned long fs::
propergation() const noexcept
{
    assert(m_handle);

    unsigned long result {};

    ::mnt_fs_get_propagation(m_handle, &result);

    return result;
}

opt<cstring_view> fs::
attributes() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_attributes(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<std::string_view> fs::
attribute(cstring_view const name) const
{
    assert(m_handle);

    char* value {};
    size_t len {};
    opt<std::string_view> result;

    auto const rc = ::mnt_fs_get_attribute(m_handle, name, &value, &len);
    if (rc == 0) {
        result.emplace(value, len);
    }
    else if (rc == 1) {
        // nop
    }
    else {
        assert(rc < 0);
        throw error {
            "mnt_fs_get_attribute",
            make_error_code(-rc), {
                { "name", name }
            }
        };
    }

    return result;
}

int fs::
freq() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_freq(m_handle);
}

int fs::
passno() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_passno(m_handle);
}

opt<cstring_view> fs::
root() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_root(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> fs::
bindsrc() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_bindsrc(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

int fs::
id() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_id(m_handle);
}

int fs::
parent_id() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_parent_id(m_handle);
}

::dev_t fs::
devno() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_devno(m_handle);
}

::pid_t fs::
tid() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_tid(m_handle);
}

opt<cstring_view> fs::
swaptype() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_swaptype(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

::off_t fs::
size() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_size(m_handle);
}

::off_t fs::
usedsize() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_usedsize(m_handle);
}

int fs::
priority() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_get_priority(m_handle);
}

opt<cstring_view> fs::
comment() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_fs_get_comment(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

bool fs::
match_fstype(cstring_view const types) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_match_fstype(m_handle, types);
}

bool fs::
match_options(cstring_view const options) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_match_options(m_handle, options);
}

bool fs::
is_kernel() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_is_kernel(m_handle);
}

bool fs::
is_swaparea() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_is_swaparea(m_handle);
}

bool fs::
is_netfs() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_is_netfs(m_handle);
}

bool fs::
is_pseudofs() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_is_pseudofs(m_handle);
}

#if 0
bool fs::
is_regularfs() const noexcept
{
    assert(m_handle);

    return ::mnt_fs_is_regularfs(m_handle);
}
#endif

void fs::
set_userdata(void* const data) noexcept
{
    assert(m_handle);

    ::mnt_fs_set_userdata(m_handle, data);
}

void fs::
set_source(cstring_view const source)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_source(m_handle, source);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_source()",
            make_error_code(-rc), {
                { "source", source }
            }
        };
    }
}

void fs::
set_target(cstring_view const target)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_target(m_handle, target);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_target()",
            make_error_code(-rc), {
                { "target", target }
            }
        };
    }
}

void fs::
set_fstype(cstring_view const fstype)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_fstype(m_handle, fstype);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_fstype()",
            make_error_code(-rc), {
                { "fstype", fstype }
            }
        };
    }
}

void fs::
set_options(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_options(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_options()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void fs::
append_options(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_append_options(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_fs_append_options()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void fs::
prepend_options(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_prepend_options(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_fs_prepend_options()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void fs::
set_attributes(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_attributes(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_attributes()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void fs::
append_attributes(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_append_attributes(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_fs_append_attributes()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void fs::
prepend_attributes(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_prepend_attributes(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_fs_prepend_attributes()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void fs::
set_freq(int const freq) noexcept
{
    assert(m_handle);

    ::mnt_fs_set_freq(m_handle, freq);
}

void fs::
set_passno(int const passno) noexcept
{
    assert(m_handle);

    ::mnt_fs_set_passno(m_handle, passno);
}

void fs::
set_root(cstring_view const path)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_root(m_handle, path);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_root()",
            make_error_code(-rc), {
                { "path", path }
            }
        };
    }
}

void fs::
set_bindsrc(cstring_view const path)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_bindsrc(m_handle, path);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_bindsrc()",
            make_error_code(-rc), {
                { "path", path }
            }
        };
    }
}

void fs::
set_priority(int const prio) noexcept
{
    assert(m_handle);

    ::mnt_fs_set_priority(m_handle, prio);
}

void fs::
set_comment(cstring_view const comm)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_set_comment(m_handle, comm);
    if (rc < 0) {
        throw error {
            "mnt_fs_set_comment()",
            make_error_code(-rc), {
                { "comm", comm }
            }
        };
    }
}

void fs::
append_comment(cstring_view const comm)
{
    assert(m_handle);

    auto const rc = ::mnt_fs_append_comment(m_handle, comm);
    if (rc < 0) {
        throw error {
            "mnt_fs_append_comment()",
            make_error_code(-rc), {
                { "comm", comm }
            }
        };
    }
}

bool fs::
streq_srcpath(cstring_view const path) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_streq_srcpath(m_handle, path);
}

bool fs::
streq_target(cstring_view const path) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_streq_target(m_handle, path);
}

bool fs::
match_source(cstring_view const source) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_match_source(m_handle, source, nullptr);
}

bool fs::
match_source(cstring_view const source, cache& c) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_match_source(m_handle, source, c);
}

bool fs::
match_target(cstring_view const target) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_match_target(m_handle, target, nullptr);
}

bool fs::
match_target(cstring_view const target, cache& c) const noexcept
{
    assert(m_handle);

    return ::mnt_fs_match_target(m_handle, target, c);
}

void fs::
reset()
{
    assert(m_handle);

    ::mnt_reset_fs(m_handle);
}

fs fs::
copy()
{
    assert(m_handle);

    auto* const h = ::mnt_copy_fs(nullptr, m_handle);
    if (h == nullptr) {
        throw error { "mnt_copy_fs()", make_error_code(errno) };
    }

    return fs { *h };
}

void fs::
copy_to(fs& dest)
{
    assert(m_handle);

    auto* const h = ::mnt_copy_fs(dest.m_handle, m_handle);
    if (h == nullptr) {
        throw error { "mnt_copy_fs()", make_error_code(errno) };
    }
}

void fs::
print_debug(FILE* const file/*= stdout*/) const
{
    assert(m_handle);

    ::mnt_fs_print_debug(m_handle, file);
}

struct mntent*
to_mntent(fs& fs, struct ::mntent* mnt/*= {}*/)
{
    auto const rc = ::mnt_fs_to_mntent(fs, &mnt);
    if (rc < 0) {
        throw error { "mnt_fs_to_mntent()", make_error_code(-rc) };
    }

    return mnt;
}

static void
insert(json::object& obj, cstring_view name, opt<cstring_view> const& o_v)
{
    if (o_v) {
        obj[name] = *o_v;
    }
    else {
        obj[name] = "";
    }
}

static void
insert_if_not_null(json::object& obj,
                   cstring_view name, opt<cstring_view> const& o_v)
{
    if (o_v) {
        obj[name] = *o_v;
    }
}

template<std::integral T>
static void
insert_if_not_zero(json::object& obj, cstring_view name, T const v)
{
    if (v) {
        obj[name] = v;
    }
}

void
tag_invoke(json::value_from_tag, json::value& v, fs const& fs)
{
    auto& o = v.emplace_object();

    insert(o, "source", fs.source());
    insert(o, "target", fs.target());
    insert(o, "fstype", fs.fstype());
    insert_if_not_null(o, "optstr", fs.options());
    insert_if_not_null(o, "VFS-optstr", fs.vfs_options());
    insert_if_not_null(o, "FS-optstr", fs.fs_options());
    insert_if_not_null(o, "user-optstr", fs.user_options());
    insert_if_not_null(o, "optional-fields", fs.optional_fields());
    insert_if_not_null(o, "attributes", fs.attributes());

    insert_if_not_null(o, "root", fs.root());

    insert_if_not_null(o, "swaptype", fs.swaptype());
    insert_if_not_zero(o, "size", fs.size());
    insert_if_not_zero(o, "usedsize", fs.usedsize());
    insert_if_not_zero(o, "priority", fs.priority());

    insert_if_not_null(o, "bindsrc", fs.bindsrc());
    insert_if_not_zero(o, "freq", fs.freq());
    insert_if_not_zero(o, "pass", fs.passno());
    insert_if_not_zero(o, "id", fs.id());
    insert_if_not_zero(o, "parent", fs.parent_id());
    insert_if_not_zero(o, "devno", fs.devno());
    insert_if_not_zero(o, "tid", fs.tid());
    insert_if_not_null(o, "comment", fs.comment());
}

std::ostream&
operator<<(std::ostream& os, fs const& fs)
{
    os << json::value_from(fs);
    return os;
}

} // namespace stream9::libmount
