#include <stream9/libmount/context.hpp>

#include "namespace.hpp"

#include <stream9/libmount/cache.hpp>
#include <stream9/libmount/error.hpp>
#include <stream9/libmount/fs.hpp>
#include <stream9/libmount/table.hpp>

#include <utility>

#include <stream9/json.hpp>

namespace stream9::libmount {

context::
context()
    : m_handle { ::mnt_new_context() }
{
    if (!m_handle) {
        throw error { "mnt_new_context()", make_error_code(errno) };
    }
}

context::
~context() noexcept
{
    if (m_handle) {
        ::mnt_free_context(m_handle);
    }
}

context::
context(context&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

context& context::
operator=(context&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(context& lhs, context& rhs) noexcept
{
    using std::swap;

    swap(lhs.m_handle, rhs.m_handle);
}

opt<cstring_view> context::
source() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_context_get_source(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> context::
target() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_context_get_target(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> context::
fstype() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_context_get_fstype(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> context::
target_prefix() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_context_get_target_prefix(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

opt<cstring_view> context::
options() const noexcept
{
    assert(m_handle);

    opt<cstring_view> result;

    auto* const s = ::mnt_context_get_options(m_handle);
    if (s) {
        result.emplace(s);
    }

    return result;
}

bool context::
is_restricted() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_restricted(m_handle);
}

bool context::
is_lazy() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_lazy(m_handle);
}

bool context::
is_rdonly_umount() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_rdonly_umount(m_handle);
}

bool context::
is_rwonly_mount() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_rwonly_mount(m_handle);
}

bool context::
is_sloppy() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_sloppy(m_handle);
}

bool context::
is_fake() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_fake(m_handle);
}

bool context::
is_nomtab() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_nomtab(m_handle);
}

bool context::
is_force() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_force(m_handle);
}

bool context::
is_verbose() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_verbose(m_handle);
}

bool context::
is_loopdel() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_loopdel(m_handle);
}

bool context::
is_nohelpers() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_nohelpers(m_handle);
}

bool context::
is_nocanonicalize() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_nocanonicalize(m_handle);
}

bool context::
is_swapmatch() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_swapmatch(m_handle);
}

bool context::
forced_rdonly() const noexcept
{
    assert(m_handle);

    return ::mnt_context_forced_rdonly(m_handle);
}

bool context::
is_fork() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_fork(m_handle);
}

bool context::
is_parent() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_parent(m_handle);
}

bool context::
is_child() const noexcept
{
    assert(m_handle);

    return ::mnt_context_is_child(m_handle);
}

bool context::
tab_applied() const noexcept
{
    assert(m_handle);

    return ::mnt_context_tab_applied(m_handle);
}

unsigned long context::
mflags() const
{
    assert(m_handle);

    unsigned long f {};

    auto const rc = ::mnt_context_get_mflags(m_handle, &f);
    if (rc < 0) {
        throw error { "mnt_context_get_mflags()", make_error_code(-rc) };
    }

    return f;
}

unsigned long context::
user_mflags() const
{
    assert(m_handle);

    unsigned long f {};

    auto const rc = ::mnt_context_get_user_mflags(m_handle, &f);
    if (rc < 0) {
        throw error { "mnt_context_get_mflags()", make_error_code(-rc) };
    }

    return f;
}

bool context::
is_fs_mounted(class fs const& f) const
{
    assert(m_handle);

    int mounted {};
    auto const rc = ::mnt_context_is_fs_mounted(m_handle, f, &mounted);
    if (rc < 0) {
        throw error { "mnt_context_is_fs_mounted()", make_error_code(-rc) };
    }

    return mounted;
}

int context::
optsmode() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_optsmode(m_handle);
}

class fs context::
fs() const
{
    assert(m_handle);

    auto* const f = ::mnt_context_get_fs(m_handle);
    if (!f) {
        throw error { "mnt_context_get_fs()", make_error_code(errno) };
    }

    using fs_ = class fs;
    return fs_ { *f };
}

void* context::
fs_userdata() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_fs_userdata(m_handle);
}

table context::
fstab() const
{
    assert(m_handle);

    ::libmnt_table* tbl {};

    auto const rc = ::mnt_context_get_fstab(m_handle, &tbl);
    if (rc < 0) {
        throw error { "mnt_context_get_fstab()", make_error_code(-rc) };
    }

    assert(tbl);
    using table_ = class table;
    return table_ { *tbl };
}

void* context::
fstab_userdata() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_fstab_userdata(m_handle);
}

table context::
mtab() const
{
    assert(m_handle);

    ::libmnt_table* tbl {};

    auto const rc = ::mnt_context_get_mtab(m_handle, &tbl);
    if (rc < 0) {
        throw error { "mnt_context_get_mtab()", make_error_code(-rc) };
    }

    assert(tbl);
    using table_ = class table;
    return table_ { *tbl };
}

void* context::
mtab_userdata() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_mtab_userdata(m_handle);
}

opt<class cache> context::
cache() const noexcept
{
    assert(m_handle);

    opt<class cache> result;

    auto* const c = ::mnt_context_get_cache(m_handle);
    if (c) {
        result.emplace(*c);
    }

    return result;
}

void context::
wait_for_children(int* nchildren, int* nerrs) const noexcept
{
    assert(m_handle);

    ::mnt_context_wait_for_children(m_handle, nchildren, nerrs);
}

class table context::
table(cstring_view const filename) const
{
    assert(m_handle);

    ::libmnt_table* tb {};
    auto const rc = ::mnt_context_get_table(m_handle, filename, &tb);
    if (rc < 0) {
        throw error {
            "mnt_context_get_table()",
            make_error_code(-rc), {
                { "filename", filename }
            }
        };
    }

    assert(tb);
    using table_ = class table;
    return table_ { *tb };
}

bool context::
status() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_status(m_handle);
}

bool context::
helper_executed() const noexcept
{
    assert(m_handle);

    return ::mnt_context_helper_executed(m_handle);
}

int context::
helper_status() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_helper_status(m_handle);
}

bool context::
syscall_called() const noexcept
{
    assert(m_handle);

    return ::mnt_context_syscall_called(m_handle);
}

int context::
syscall_errno() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_syscall_errno(m_handle);
}

int context::
excode(int const rc, std::span<char> buf) const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_excode(m_handle, rc, buf.data(), buf.size());
}

::libmnt_lock* context::
lock() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_lock(m_handle);
}

::libmnt_ns* context::
target_ns() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_target_ns(m_handle);
}

::libmnt_ns* context::
origin_ns() const noexcept
{
    assert(m_handle);

    return ::mnt_context_get_target_ns(m_handle);
}

void context::
set_source(cstring_view const source)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_source(m_handle, source);
    if (rc < 0) {
        throw error {
            "mnt_context_set_source()",
            make_error_code(-rc), {
                { "source", source }
            }
        };
    }
}

void context::
set_target(cstring_view const target)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_target(m_handle, target);
    if (rc < 0) {
        throw error {
            "mnt_context_set_target()",
            make_error_code(-rc), {
                { "target", target }
            }
        };
    }
}

void context::
set_fstype(cstring_view const fstype)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_fstype(m_handle, fstype);
    if (rc < 0) {
        throw error {
            "mnt_context_set_fstype()",
            make_error_code(-rc), {
                { "fstype", fstype }
            }
        };
    }
}

void context::
set_target_prefix(cstring_view const path)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_target_prefix(m_handle, path);
    if (rc < 0) {
        throw error {
            "mnt_context_set_target_prefix()",
            make_error_code(-rc), {
                { "path", path }
            }
        };
    }
}

void context::
set_options(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_options(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_context_set_options()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void context::
append_options(cstring_view const optstr)
{
    assert(m_handle);

    auto const rc = ::mnt_context_append_options(m_handle, optstr);
    if (rc < 0) {
        throw error {
            "mnt_context_append_options()",
            make_error_code(-rc), {
                { "optstr", optstr }
            }
        };
    }
}

void context::
enable_lazy(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_lazy(m_handle, enable);
}

void context::
enable_rdonly_umount(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_rdonly_umount(m_handle, enable);
}

void context::
enable_rwonly_mount(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_rwonly_mount(m_handle, enable);
}

void context::
enable_sloppy(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_sloppy(m_handle, enable);
}

void context::
enable_fake(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_fake(m_handle, enable);
}

void context::
disable_mtab(bool const disable) noexcept
{
    assert(m_handle);

    ::mnt_context_disable_mtab(m_handle, disable);
}

void context::
enable_force(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_force(m_handle, enable);
}

void context::
enable_verbose(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_verbose(m_handle, enable);
}

void context::
enable_loopdel(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_loopdel(m_handle, enable);
}

void context::
disable_helpers(bool const disable) noexcept
{
    assert(m_handle);

    ::mnt_context_disable_helpers(m_handle, disable);
}

void context::
disable_canonicalize(bool const disable) noexcept
{
    assert(m_handle);

    ::mnt_context_disable_canonicalize(m_handle, disable);
}

void context::
disable_swapmatch(bool const disable) noexcept
{
    assert(m_handle);

    ::mnt_context_disable_swapmatch(m_handle, disable);
}

void context::
enable_fork(bool const enable) noexcept
{
    assert(m_handle);

    ::mnt_context_enable_fork(m_handle, enable);
}

void context::
set_mflags(unsigned long const flags)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_mflags(m_handle, flags);
    if (rc < 0) {
        throw error {
            "mnt_context_set_mflags",
            make_error_code(-rc), {
                { "flags", flags },
            }
        };
    }
}

void context::
set_user_mflags(unsigned long const flags)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_user_mflags(m_handle, flags);
    if (rc < 0) {
        throw error {
            "mnt_context_set_user_mflags",
            make_error_code(-rc), {
                { "flags", flags },
            }
        };
    }
}

void context::
set_optsmode(int const mode) noexcept
{
    assert(m_handle);

    ::mnt_context_set_optsmode(m_handle, mode);
}

void context::
set_fs(class fs const& f) noexcept
{
    assert(m_handle);

    ::mnt_context_set_fs(m_handle, f);
}

void context::
set_fstab(class table const& tb) noexcept
{
    assert(m_handle);

    ::mnt_context_set_fstab(m_handle, tb);
}

void context::
set_cache(class cache const& c) noexcept
{
    assert(m_handle);

    ::mnt_context_set_cache(m_handle, c);
}

void context::
set_fstype_pattern(cstring_view const pattern)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_fstype_pattern(m_handle, pattern);
    if (rc < 0) {
        throw error {
            "mnt_context_set_fstype_pattern()",
            make_error_code(-rc), {
                { "pattern", pattern }
            }
        };
    }
}

void context::
set_options_pattern(cstring_view const pattern)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_options_pattern(m_handle, pattern);
    if (rc < 0) {
        throw error {
            "mnt_context_set_options_pattern()",
            make_error_code(-rc), {
                { "pattern", pattern }
            }
        };
    }
}

void context::
set_mountdata(void* const data) noexcept
{
    assert(m_handle);

    ::mnt_context_set_mountdata(m_handle, data);
}

void context::
force_unrestricted() noexcept
{
    assert(m_handle);

    ::mnt_context_force_unrestricted(m_handle);
}

void context::
init_helper(int const action, int const flags) noexcept
{
    assert(m_handle);

    ::mnt_context_init_helper(m_handle, action, flags);
}

bool context::
helper_setopt(int const c, char* const arg) noexcept
{
    assert(m_handle);

    return ::mnt_context_helper_setopt(m_handle, c, arg);
}

int context::
apply_fstab() noexcept
{
    assert(m_handle);

    return ::mnt_context_apply_fstab(m_handle);
}

void context::
reset() noexcept
{
    assert(m_handle);

    ::mnt_reset_context(m_handle);
}

void context::
reset_status() noexcept
{
    assert(m_handle);

    ::mnt_context_reset_status(m_handle);
}

void context::
set_syscall_status(int const status) noexcept
{
    assert(m_handle);

    ::mnt_context_set_syscall_status(m_handle, status);
}

void context::
set_tables_errcb(
    int (*cb)(struct ::libmnt_table* tb, char const* filename, int line) ) noexcept
{
    assert(m_handle);

    ::mnt_context_set_tables_errcb(m_handle, cb);
}

void context::
set_target_ns(cstring_view const path)
{
    assert(m_handle);

    auto const rc = ::mnt_context_set_target_ns(m_handle, path);
    if (rc < 0) {
        throw error { "mnt_context_set_target_ns()", make_error_code(-rc) };
    }
}

::libmnt_ns* context::
switch_origin_ns() noexcept
{
    assert(m_handle);

    return ::mnt_context_switch_origin_ns(m_handle);
}

::libmnt_ns* context::
switch_target_ns() noexcept
{
    assert(m_handle);

    return ::mnt_context_switch_target_ns(m_handle);
}

} // namespace stream9::libmount
