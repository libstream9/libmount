#include <stream9/libmount/monitor.hpp>

#include "namespace.hpp"

#include <stream9/libmount/error.hpp>

#include <utility>

#include <stream9/json.hpp>

namespace stream9::libmount {

monitor::
monitor()
    : m_handle { ::mnt_new_monitor() }
{
    if (!m_handle) {
        throw error { "mnt_new_monitor()", make_error_code(errno) };
    }
}

monitor::
~monitor() noexcept
{
    if (m_handle) {
        ::mnt_unref_monitor(m_handle);
    }
}

monitor::
monitor(monitor const& other) noexcept
    : m_handle { other.m_handle }
{
    ::mnt_ref_monitor(m_handle);
}

monitor& monitor::
operator=(monitor const& other) noexcept
{
    monitor tmp { other };
    swap(*this, tmp);

    return *this;
}

monitor::
monitor(monitor&& other) noexcept
    : m_handle { other.m_handle }
{
    other.m_handle = nullptr;
}

monitor& monitor::
operator=(monitor&& other) noexcept
{
    swap(*this, other);

    return *this;
}

void
swap(monitor& lhs, monitor& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_handle, rhs.m_handle);
}

int monitor::
fd() const
{
    assert(m_handle);

    auto const rc = ::mnt_monitor_get_fd(m_handle);
    if (rc < 0) {
        throw error { "mnt_monitor_get_fd()", make_error_code(-rc) };
    }

    return rc;
}

bool monitor::
wait(duration const timeout/*= -1*/) const
{
    assert(m_handle);

    auto const rc = ::mnt_monitor_wait(m_handle, timeout.count());
    if (rc < 0) {
        throw error {
            "mnt_monitor_wait()",
            make_error_code(-rc), {
                { "timeout", timeout.count() }
            }
        };
    }

    return rc;
}

opt<monitor::next_change_result> monitor::
next_change() const
{
    assert(m_handle);

    opt<next_change_result> result;
    char const* filename {};
    int type {};

    auto const rc = ::mnt_monitor_next_change(m_handle, &filename, &type);
    if (rc == 0) {
        assert(filename);
        result.emplace(filename, type);
    }
    else if (rc < 0) {
        throw error { "mnt_monitor_next_change()", make_error_code(-rc) };
    }

    return result;
}

void monitor::
enable_kernel(bool const enable/*= true*/)
{
    assert(m_handle);

    auto const rc = ::mnt_monitor_enable_kernel(m_handle, enable);
    if (rc < 0) {
        throw error {
            "mnt_monitor_enable_kernel",
            make_error_code(-rc), {
                { "enable", enable },
            }
        };
    }
}

void monitor::
enable_userspace(bool const enable/*= true*/, opt<cstring_view> const filename/*= {}*/)
{
    assert(m_handle);

    auto const rc = ::mnt_monitor_enable_userspace(
                      m_handle, enable, filename ? filename->c_str() : nullptr);
    if (rc < 0) {
        json::object cxt { { "enable", enable } };
        if (filename) {
            cxt["filename"] = filename->c_str();
        }

        throw error {
            "mnt_monitor_enable_userspace",
            make_error_code(-rc),
            std::move(cxt)
        };
    }
}

void monitor::
cleanup()
{
    assert(m_handle);

    auto const rc = ::mnt_monitor_event_cleanup(m_handle);
    if (rc < 0) {
        throw error { "mnt_monitor_event_cleanup()", make_error_code(-rc) };
    }
}

} // namespace stream9::libmount
