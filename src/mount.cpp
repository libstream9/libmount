#include <stream9/libmount/mount.hpp>

#include <stream9/libmount/context.hpp>
#include <stream9/libmount/error.hpp>
#include <stream9/libmount/iter.hpp>

#include <libmount.h>

namespace stream9::libmount {

void
mount(context& c)
{
    auto const rc = ::mnt_context_mount(c);
    if (rc < 0) {
        throw error { "mnt_context_mount()", make_error_code(-rc) };
    }
}

next_mount_result
next_mount(context& c, iter& i)
{
    ::libmnt_fs* f {};
    int mntrc;
    int ignored;

    auto const rc = ::mnt_context_next_mount(c, i, &f, &mntrc, &ignored);
    if (rc < 0) {
        throw error { "mnt_context_next_mount()", make_error_code(-rc) };
    }

    return {
        fs { *f },
        mntrc,
        ignored
    };
}

next_mount_result
next_remount(context& c, iter& i)
{
    ::libmnt_fs* f {};
    int mntrc;
    int ignored;

    auto const rc = ::mnt_context_next_remount(c, i, &f, &mntrc, &ignored);
    if (rc < 0) {
        throw error { "mnt_context_next_remount()", make_error_code(-rc) };
    }

    return {
        fs { *f },
        mntrc,
        ignored
    };
}

} // namespace stream9::libmount
